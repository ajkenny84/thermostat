/* Thermostat.ino - Adam Kenny
Allows a user to set a desired temperature.
  When the desired temp is reached a light will turn on.
Updated on 2013-07-01 */

/*
PIN CONNECTIONS
ARDUINO   RESISTOR  TRIMPOT   TMP   LED           5V  GRND
2         330                       + (anode)
                                    - (cathode)       Grnd
A0                            2
                              1                   5v
                              3                       Grnd
A1                  2
                    1                             5v
                    3                                 Grnd
*/

// set input pins for temp and pot sensors
const int tempPin = 0;
const int potPin = 1;

const int ledPin = 2; // set the output pin

// set the range of temps our potentiometer can adjust to
int lowTemp = 50;
int highTemp = 100;

int delayTimer = 10000; // time to delay in ms, prevents rapid on-off pattern

void setup(){
    Serial.begin(9600); // opens a serial port to see the data on a computer
  
    pinMode(ledPin, OUTPUT); // sets the LED pin to be used as output
}

void loop(){
    // create the variables that we will be using
    float voltage, degreesC, degreesF;
    int potValue;

    // convert the temperature pin readings to usable values
    voltage = getVoltage(tempPin);
    degreesC = (voltage - 0.5) * 100.0;
    degreesF = degreesC * (9.0/5.0) + 32.0;

    potValue = analogRead(potPin); // get the value of the potentiometer

    // maps and constrains potentiometer value to fit within our temp range
    potValue = map(potValue, 0, 1023, lowTemp, highTemp);
    potValue = constrain(potValue, lowTemp, highTemp);

    // prints out our desired and actual temp values on screen
    Serial.print("Desired Temp: ");
    Serial.print(potValue);
    Serial.print(" Actual Temp: ");
    Serial.println(degreesF, 1); // show temp using one number after decimal
  
    if (degreesF > potValue){
        tempAbove();
    }else{
        tempBelow();
    }
  
    delay(delayTimer); // wait before printing out another line
}

// function that runs when desired temp is reached
void tempAbove(){
    digitalWrite(ledPin, HIGH); // light up the LED
}

void tempBelow(){
    digitalWrite(ledPin, LOW); // turn off the LED
}

// function that converts pin readings into actual voltage values
float getVoltage(int pin){
    return (analogRead(pin) * 0.004882814);
}